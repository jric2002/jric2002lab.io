/* General */
function genRanNum(min, max) {
  min = (typeof(min) == "undefined") ? 1 : min;
  max = (typeof(min) == "undefined") ? 2 : max;
  return Math.round((Math.random() * (max - min)) + min);
}
/* Theme Dark and Light */
const THEME = document.getElementById("theme");
const THEME_BUTTON = document.getElementById("theme-button");
const THEME_ICON = document.getElementById("theme-icon");
//const USER_THEME = window.matchMedia("(prefers-color-scheme: dark)").matches;
var default_theme = "dark"; // Default theme
var user_theme = (window.localStorage.getItem("theme") != null) ? window.localStorage.getItem("theme") : default_theme; // User theme
// var user_theme = window.localStorage.getItem("theme") ?? default_theme; // User theme
var theme = {
  dark: function() {
    window.localStorage.setItem("theme", user_theme);
    user_theme = "light";
    document.body.classList.remove("light-theme");
    document.body.classList.add("dark-theme");
    THEME_BUTTON.innerHTML = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"moon\" class=\"svg-inline--fa fa-moon fa-w-16\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path fill=\"currentColor\" d=\"M283.211 512c78.962 0 151.079-35.925 198.857-94.792 7.068-8.708-.639-21.43-11.562-19.35-124.203 23.654-238.262-71.576-238.262-196.954 0-72.222 38.662-138.635 101.498-174.394 9.686-5.512 7.25-20.197-3.756-22.23A258.156 258.156 0 0 0 283.211 0c-141.309 0-256 114.511-256 256 0 141.309 114.511 256 256 256z\"></path></svg>";
    THEME_BUTTON.classList.remove("light-theme");
    THEME_BUTTON.classList.add("dark-theme");
  },
  light: function() {
    window.localStorage.setItem("theme", user_theme);
    user_theme = "dark";
    document.body.classList.remove("dark-theme");
    document.body.classList.add("light-theme");
    THEME_BUTTON.innerHTML = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"sun\" class=\"svg-inline--fa fa-sun fa-w-16\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path fill=\"currentColor\" d=\"M256 160c-52.9 0-96 43.1-96 96s43.1 96 96 96 96-43.1 96-96-43.1-96-96-96zm246.4 80.5l-94.7-47.3 33.5-100.4c4.5-13.6-8.4-26.5-21.9-21.9l-100.4 33.5-47.4-94.8c-6.4-12.8-24.6-12.8-31 0l-47.3 94.7L92.7 70.8c-13.6-4.5-26.5 8.4-21.9 21.9l33.5 100.4-94.7 47.4c-12.8 6.4-12.8 24.6 0 31l94.7 47.3-33.5 100.5c-4.5 13.6 8.4 26.5 21.9 21.9l100.4-33.5 47.3 94.7c6.4 12.8 24.6 12.8 31 0l47.3-94.7 100.4 33.5c13.6 4.5 26.5-8.4 21.9-21.9l-33.5-100.4 94.7-47.3c13-6.5 13-24.7.2-31.1zm-155.9 106c-49.9 49.9-131.1 49.9-181 0-49.9-49.9-49.9-131.1 0-181 49.9-49.9 131.1-49.9 181 0 49.9 49.9 49.9 131.1 0 181z\"></path></svg>";
    THEME_BUTTON.classList.remove("dark-theme");
    THEME_BUTTON.classList.add("light-theme");
  }
};
if (user_theme == "dark") {
  theme.dark();
}
else {
  theme.light();
}
THEME.addEventListener("click", function() {
  if (user_theme == "dark") {
    theme.dark();
  }
  else {
    theme.light();
  }
});

/* Presentation */
/* Scroll */
const BACK_TO_TOP = document.getElementById("back-to-top");
window.addEventListener("scroll", function() {
  if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
    BACK_TO_TOP.classList.add("show-back-button");
  }
  else {
    BACK_TO_TOP.classList.remove("show-back-button");
  }
});
function scrollUpdate() {
  var current_scroll = document.body.scrollTop || document.documentElement.scrollTop;
  if (current_scroll > 0) {
    window.scrollTo(0, current_scroll - (current_scroll / 7.5));
    window.requestAnimationFrame(scrollUpdate);
  }
}
BACK_TO_TOP.addEventListener("click", scrollUpdate);
/** Header background **/
const HEADER = document.getElementById("header");
window.addEventListener("scroll", function() {
  if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
    HEADER.classList.add("header-background");
  }
  else {
    HEADER.classList.remove("header-background");
  }
});

/* Start */
/* const BACKGROUND = document.getElementById("background");
const BACKGROUND_IMAGES = ["cyberpunk.jpg", "street.jpg", "city-cyberpunk.jpg", "desktop-hacking-cyberpunk.jpg"];
const RANDOM_BACKGROUND_NUMBER = genRanNum(0, BACKGROUND_IMAGES.length - 1);
BACKGROUND.setAttribute("src", "./img/wallpapers/" + BACKGROUND_IMAGES[RANDOM_BACKGROUND_NUMBER]); */
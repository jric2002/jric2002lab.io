/* General */
const ARCH_LINUX = document.getElementById("arch-linux");
const WALLPAPERS = ["storm.jpg", "aesthetic-black-laptop-1160x774.jpg", "street.jpg", "desktop-hacking-cyberpunk.jpg"];
var current_wallpaper_position = 1;
/* setInterval(function() {
  if (current_wallpaper_position >= WALLPAPERS.length) {
    current_wallpaper_position = 0;
  }
  ARCH_LINUX.style.backgroundImage = "url(\"./img/wallpapers/" + WALLPAPERS[current_wallpaper_position] + "\")";
  current_wallpaper_position++;
}, (30 * 1000)); */
/** Menu **/
const MENU = document.getElementById("menu");
const ARCH_LINUX_LOGO = document.getElementById("arch-linux-logo");
const DESK = document.getElementById("desk");
const OPTIONS = document.getElementById("options");
const VOL = document.getElementById("vol").querySelector("span");
// const MEMORY_RAM = (document.getElementById("memory-ram")).querySelector("span");
var time = document.getElementById("time");
// MEMORY_RAM.innerHTML = navigator.deviceMemory;
setInterval(() => {
  let d = new Date();
  time.innerHTML = d.toLocaleTimeString([], {hour12: true, hour: "2-digit", minute: "2-digit"});
}, 1000);
ARCH_LINUX_LOGO.addEventListener("click", function(event) {
  let s1_sm1 = MENU.querySelector("div.set-1 > div.sub-menu-1");
  if (window.innerWidth <= 768) {
    if (s1_sm1.style.display == "none" || s1_sm1.style.display == "") {
      s1_sm1.style.display = "flex";
    }
    else {
      s1_sm1.style.display = "none";
    }
  }
});
DESK.addEventListener("click", function(event) {
  let s2_sm1 = MENU.querySelector("div.set-2 > div.sub-menu-1");
  if (window.innerWidth <= 1024) {
    if (s2_sm1.style.display == "none" || s2_sm1.style.display == "") {
      s2_sm1.style.display = "flex";
    }
    else {
      s2_sm1.style.display = "none";
    }
  }
});
OPTIONS.addEventListener("click", function(event) {
  let s3_sm1 = MENU.querySelector("div.set-3 > div.sub-menu-1");
  if (window.innerWidth <= 768) {
    if (s3_sm1.style.display == "none" || s3_sm1.style.display == "") {
      s3_sm1.style.display = "flex";
    }
    else {
      s3_sm1.style.display = "none";
    }
  }
});

/** Music Player **/
/*** Song ***/
const SONG = document.getElementById("song");
const ARTIST = document.getElementById("artist");
const COVER_ART_IMAGE = document.getElementById("cover-art-image").querySelector("img");
function song(title, artist, cover_art_image, url) {
  this.title = title;
  this.artist = artist;
  this.cover_art_image = cover_art_image;
  this.url = url;
}
const SONGS = [
  new song(
    "Good Life",
    "OneRepublic",
    "https://i1.sndcdn.com/artworks-000043031409-33pw3f-t500x500.jpg",
    "https://files.catbox.moe/oqcdax.mp3"
  ),
  new song(
    "Take a Walk",
    "Passion Pit",
    "https://i1.sndcdn.com/artworks-000076371658-d19gh1-t500x500.jpg",
    "https://files.catbox.moe/jz5r97.mp3"
  ),
  new song(
    "Stereo Hearts",
    "Gym Class Heroes, Adam Levine",
    "https://i.scdn.co/image/ab67616d0000b27318b8088fe0c3dbf78398b55a",
    "https://files.catbox.moe/msfa96.mp3"
  )
  /* new song(
    "Monody",
    "TheFatRat, Laura Brehm",
    "https://i.scdn.co/image/ab67616d00001e02f0f3a191d7dcaf2b6a8ac86c",
    "https://files.catbox.moe/1xay54.mp3"
  ),
  new song(
    "Windfall",
    "TheFatRat",
    "https://i.scdn.co/image/ab67616d00001e02f7f908791a25ded26b4ac7e9",
    "https://files.catbox.moe/4f2hhy.mp3"
  ) */
];
let current_song_position = 0;
const AUDIO = new Audio();
AUDIO.volume = 0.5;
let audio_current_volume = AUDIO.volume;
VOL.innerHTML = AUDIO.volume * 100;
SONG.innerHTML = SONGS[current_song_position].title;
ARTIST.innerHTML = SONGS[current_song_position].artist;
COVER_ART_IMAGE.src = SONGS[current_song_position].cover_art_image;
AUDIO.src = SONGS[current_song_position].url;
AUDIO.addEventListener("loadedmetadata", function(event) {
  PLAYBACK_DURATION.innerHTML = Math.floor(AUDIO.duration/60).toString().padStart(2, '0') + ":" + Math.round(AUDIO.duration%60).toString().padStart(2, '0');
});
AUDIO.cp_seeking = false; // Custom property
AUDIO.addEventListener("timeupdate", function(event) {
  if (!AUDIO.cp_seeking) {
    PLAYBACK_POSITION.innerHTML = Math.floor(AUDIO.currentTime/60).toString().padStart(2, '0') + ":" + Math.round(AUDIO.currentTime%60).toString().padStart(2, '0');
    let progress_width = (AUDIO.currentTime * PLAYBACK_PROGRESSBAR.getBoundingClientRect().width)/AUDIO.duration;
    if (progress_width < 0) {
      progress_width = 0;
    }
    if (progress_width > PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) {
      progress_width = PLAYBACK_PROGRESSBAR.getBoundingClientRect().width;
    }
    PLAYBACK_PROGRESSBAR_PROGRESS.style.width = ((progress_width/PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) * 100).toFixed(2) + "%";
    PLAYBACK_PROGRESSBAR_THUMB.style.left = (progress_width - (PLAYBACK_PROGRESSBAR_THUMB.getBoundingClientRect().width/2)).toString() + "px";
  }
});
AUDIO.addEventListener("ended", function(event) {
  PAUSE.style.display = "none";
  PLAY.style.display = "flex";
});
/*** General Controls ***/
const CONTROL_BUTTON_SKIP_BACK = document.getElementById("control-button-skip-back");
const PLAY = document.getElementById("play");
const PAUSE = document.getElementById("pause");
const CONTROL_BUTTON_SKIP_FORWARD = document.getElementById("control-button-skip-forward");
CONTROL_BUTTON_SKIP_BACK.addEventListener("click", function(event) {
  if (current_song_position == 0) {
    current_song_position = (SONGS.length - 1);
  }
  else {
    current_song_position -= 1;
  }
  SONG.innerHTML = SONGS[current_song_position].title;
  ARTIST.innerHTML = SONGS[current_song_position].artist;
  COVER_ART_IMAGE.src = SONGS[current_song_position].cover_art_image;
  AUDIO.src = SONGS[current_song_position].url;
  AUDIO.play();
  PLAY.style.display = "none";
  PAUSE.style.display = "flex";
});
PLAY.addEventListener("click", function(event) {
  AUDIO.play();
  PLAY.style.display = "none";
  PAUSE.style.display = "flex";
});
PAUSE.addEventListener("click", function(event) {
  AUDIO.pause();
  PAUSE.style.display = "none";
  PLAY.style.display = "flex";
});
CONTROL_BUTTON_SKIP_FORWARD.addEventListener("click", function(event) {
  if (current_song_position == (SONGS.length - 1)) {
    current_song_position = 0;
  }
  else {
    current_song_position += 1;
  }
  SONG.innerHTML = SONGS[current_song_position].title;
  ARTIST.innerHTML = SONGS[current_song_position].artist;
  COVER_ART_IMAGE.src = SONGS[current_song_position].cover_art_image;
  AUDIO.src = SONGS[current_song_position].url;
  AUDIO.play();
  PLAY.style.display = "none";
  PAUSE.style.display = "flex";
});
document.addEventListener("keydown", function(event) {
  /* if (event.keyCode >= 48 && event.keyCode <= 57) { // Key 0 to 9
    let temp_audio_time = AUDIO.duration/9;
    for (let i = 0; i <= 9; i++) {
      if (event.keyCode == (i + 48)) {
        if (i == 9) {
          AUDIO.currentTime = AUDIO.duration;
        }
        else {
          AUDIO.currentTime = i * temp_audio_time;
        }
        break;
      }
    }
  } */
  if (event.keyCode == 37 /* || event.keyCode == 65 */ || event.keyCode == 74) { // ArrowLeft = 37, Key A = 65, Key J = 74
    let audio_current_time = AUDIO.currentTime - 10;
    if (audio_current_time < 0) {
      audio_current_time = 0;
    }
    AUDIO.currentTime = audio_current_time;
  }
  if (event.keyCode == 32 || event.keyCode == 75 /* || event.keyCode == 81 */) { // Space = 32, Key K = 75, Key Q = 81
    if (AUDIO.paused) {
      AUDIO.play();
      PLAY.style.display = "none";
      PAUSE.style.display = "flex";
    }
    else {
      AUDIO.pause();
      PAUSE.style.display = "none";
      PLAY.style.display = "flex";
    }
  }
  if (event.keyCode == 39 /* || event.keyCode == 68 */ || event.keyCode == 76) { // ArrowRight = 39, Key D = 68, Key L
    let audio_current_time = AUDIO.currentTime + 10;
    if (audio_current_time > AUDIO.duration) {
      audio_current_time = AUDIO.duration;
    }
    AUDIO.currentTime = audio_current_time;
  }
  if (event.keyCode == 38 /* || event.keyCode == 87 */) { // ArrowUp = 38, Key W = 87
    let temp_audio_volume = AUDIO.volume + 0.1;
    if (temp_audio_volume > 1) {
      temp_audio_volume = 1;
    }
    AUDIO.volume = parseFloat(temp_audio_volume.toFixed(2));
    VOL.innerHTML = AUDIO.volume * 100;
  }
  if (event.keyCode == 40 /* || event.keyCode == 83 */) { // ArrowDown = 40, Key S = 83
    let temp_audio_volume = AUDIO.volume - 0.1;
    if (temp_audio_volume < 0) {
      temp_audio_volume = 0;
    }
    AUDIO.volume = parseFloat(temp_audio_volume.toFixed(2));
    VOL.innerHTML = AUDIO.volume * 100;
  }
  if (event.keyCode == 77) { // key M = 77
    if (AUDIO.volume == 0) {
      AUDIO.volume = audio_current_volume;
      VOL.innerHTML = AUDIO.volume * 100;
    }
    else {
      audio_current_volume = AUDIO.volume;
      AUDIO.volume = 0;
      VOL.innerHTML = AUDIO.volume;
    }
  }
});
/*** Playback Bar ***/
const PLAYBACK_POSITION = document.getElementById("playback-position");
const PLAYBACK_DURATION = document.getElementById("playback-duration");
const PLAYBACK_PROGRESSBAR = document.getElementById("playback-progressbar");
const PLAYBACK_PROGRESSBAR_PROGRESS = document.getElementById("playback-progressbar-progress");
const PLAYBACK_PROGRESSBAR_THUMB = document.getElementById("playback-progressbar-thumb");
PLAYBACK_PROGRESSBAR_THUMB.addEventListener("mousedown", function(event) {
  event.preventDefault();
  let progress_width;
  let audio_current_time;
  AUDIO.cp_seeking = true;
  function mouseMove(event){
    progress_width = event.clientX - PLAYBACK_PROGRESSBAR.getBoundingClientRect().x;
    if (progress_width < 0) {
      progress_width = 0;
    }
    if (progress_width > PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) {
      progress_width = PLAYBACK_PROGRESSBAR.getBoundingClientRect().width;
    }
    audio_current_time = (AUDIO.duration * progress_width)/PLAYBACK_PROGRESSBAR.getBoundingClientRect().width;
    PLAYBACK_POSITION.innerHTML = Math.floor(audio_current_time/60).toString().padStart(2, '0') + ":" + Math.round(audio_current_time%60).toString().padStart(2, '0');
    PLAYBACK_PROGRESSBAR_PROGRESS.style.width = ((progress_width/PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) * 100).toFixed(2) + "%";
    PLAYBACK_PROGRESSBAR_THUMB.style.left = (progress_width - (PLAYBACK_PROGRESSBAR_THUMB.getBoundingClientRect().width/2)).toString() + "px";
  }
  function mouseUp(event) {
    AUDIO.currentTime = audio_current_time;
    AUDIO.cp_seeking = false;
    document.removeEventListener("mousemove", mouseMove);
    document.removeEventListener("mouseup", mouseUp);
  }
  document.addEventListener("mousemove", mouseMove);
  document.addEventListener("mouseup", mouseUp);
});
PLAYBACK_PROGRESSBAR_THUMB.addEventListener("touchstart", function (event) {
  event.preventDefault();
  let progress_width;
  let audio_current_time;
  AUDIO.cp_seeking = true;
  function touchMove(event){
    progress_width = event.touches[0].clientX - PLAYBACK_PROGRESSBAR.getBoundingClientRect().x;
    if (progress_width < 0) {
      progress_width = 0;
    }
    if (progress_width > PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) {
      progress_width = PLAYBACK_PROGRESSBAR.getBoundingClientRect().width;
    }
    audio_current_time = (AUDIO.duration * progress_width)/PLAYBACK_PROGRESSBAR.getBoundingClientRect().width;
    PLAYBACK_POSITION.innerHTML = Math.floor(audio_current_time/60).toString().padStart(2, '0') + ":" + Math.round(audio_current_time%60).toString().padStart(2, '0');
    PLAYBACK_PROGRESSBAR_PROGRESS.style.width = ((progress_width/PLAYBACK_PROGRESSBAR.getBoundingClientRect().width) * 100).toFixed(2) + "%";
    PLAYBACK_PROGRESSBAR_THUMB.style.left = (progress_width - (PLAYBACK_PROGRESSBAR_THUMB.getBoundingClientRect().width/2)).toString() + "px";
  }
  function touchEnd(event) {
    AUDIO.currentTime = audio_current_time;
    AUDIO.cp_seeking = false;
    document.removeEventListener("touchmove", touchMove);
    document.removeEventListener("touchend", touchEnd);
  }
  document.addEventListener("touchmove", touchMove);
  document.addEventListener("touchend", touchEnd);
});
/* Header */
class Header extends HTMLElement {
  constructor() {
    super();
  }
}
window.customElements.define("jric2002-header", Header);

/* Information */
class Information extends HTMLElement {
  constructor() {
    super();
    this.avatar = "img/avatars/Avatar.jpg";
    this.name = "José Rodolfo";
    this.username = "jric2002";
    this.description = "Hola, mi nombre es " + this.name + "🎧Listen to music💚🚀";
    this.status = "🚀 Studying";
  }
}
window.customElements.define("jric2002-information", Information);

/* Portafolio */
class Portafolio extends HTMLElement {
  constructor() {
    super();
  }
}
window.customElements.define("jric2002-portafolio", Portafolio);

/* Footer */
class Footer extends HTMLElement {
  constructor() {
    super();
  }
}
window.customElements.define("jric2002-footer", Footer);